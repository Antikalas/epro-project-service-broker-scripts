#!/bin/bash
sed -i "s/localhost/localhost $(hostname)/g" /etc/hosts
~/kafka/bin/kafka-server-start.sh -daemon ~/kafka/config/server.properties > ~/kafka/kafka.log 2>&1

source try_zookeeper.sh

echo "advertised.host.name=$public_ip" >> ~/kafka/config/server.properties

tries="24"
retime="5"
res=
echo -e "ktest\n" | ~/kafka/bin/kafka-console-producer.sh --broker-list $public_ip:9092 --topic test --message-send-max-retries $tries --max-block-ms "$(( retime * 1000 ))"  

if [ "$( ~/kafka/bin/kafka-console-consumer.sh --bootstrap-server $public_ip:9092 --topic test --timeout-ms 5000 --from-beginning --max-messages 1 2>&1 | head -n 1 )" == "ktest" ]; then
  exit 0
else
  exit 3
fi