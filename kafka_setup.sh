#!/bin/bash

if uname -a | grep -q "Ubuntu" ; then
  export DEBIAN_FRONTEND=noninteractive
  apt-get --assume-yes  install -q -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" default-jre lynx zookeeperd
fi

if [ -z $distFullUrl ]; then
  [ -z $distUrl ] && distUrl="http://www.apache.org/dist/kafka/"
  [ -z $distVersion ] && distVersion=`lynx -dump -hiddenlinks=listonly  $distUrl | grep -o -e "http.*/\([0-9]\+\.\?\)*/" |  sort -r -V | grep -m1 -o -e "\([0-9]\+\.\?\)*"`
  [ -z $distScalaVersion ] && distScalaVersion="2.11"
  distFullUrl="$distUrl$distVersion/kafka_$distScalaVersion-$distVersion.tgz"
fi

source try_zookeeper.sh

mkdir -p ~/Downloads
wget $distFullUrl -O ~/Downloads/kafka.tgz
if [ $? -ne 0 ]; then
  echo "kafa download $distFullUrl failed"
  exit 3
fi

mkdir -p ~/kafka && cd ~/kafka
tar -xvzf ~/Downloads/kafka.tgz --strip 1
echo "delete.topic.enable = true" >> ~/kafka/config/server.properties
