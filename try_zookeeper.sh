n=0
while [ "$( echo -e "ruok\n" | nc localhost 2181 2>&1 )" != "imok" ] && [ "$n" -lt "24" ]; do
  (( n++ ))
  sleep 5
done

if [ "$n" -ge "24" ]; then
  exit 2;
fi