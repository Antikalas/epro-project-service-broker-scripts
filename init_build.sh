#!/bin/bash
source callback.sh
export DEBIAN_FRONTEND=noninteractive
apt-get --assume-yes upgrade -q -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold"
if [ "$?" -ne "0" ]; then
  callback "fail"
  exit 3
fi

cp /etc/hosts /etc/hosts.bak

./kafka_setup.sh
if [ "$?" -ne "0" ]; then
  callback "fail"
  exit 2
fi

./kafka_start.sh
if [ "$?" -ne "0" ]; then
  callback "fail"
  exit 1
else
  callback "success"
fi

mv /etc/hosts.bak /etc/hosts