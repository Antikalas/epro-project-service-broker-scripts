#!/bin/bash
# source callback.sh

./kafka_setup.sh
if [ "$?" -ne "0" ]; then
#  callback "fail"
  exit 2
fi

./kafka_start.sh
if [ "$?" -ne "0" ]; then
#  callback "fail"
  exit 1
fi